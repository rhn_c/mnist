import theano
from theano import tensor as T
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import numpy as np
import os
import gzip
import sys
import six.moves.cPickle as pickle
import pdb


class hiddenLayer(object):
    def __init__(self,rng, inp, n_in, n_out):
        w_vals = np.asarray(
                            rng.uniform( low = -np.sqrt(6. /(n_in + n_out)),
                                         high = np.sqrt(6. / (n_in + n_out)),
                                         size = (n_in,n_out)
                                        ),
                            dtype=theano.config.floatX
                            )
        self.W = theano.shared(value = w_vals, name = 'W', borrow = True)

        b_vals = np.zeros((n_out,), dtype=theano.config.floatX)
        self.b = theano.shared(value = b_vals, name = 'b', borrow = True)
        
        #self.W = W
        #self.b = b

        self.output = T.tanh(T.dot(inp, self.W) + self.b)
        self.input = inp
        self.params = [self.W, self.b]

class softmaxLayer(object):
    def __init__(self,inp,n_in,n_out):
        self.W = theano.shared(
                                value = np.zeros(
                                                    (n_in, n_out),
                                                    dtype = theano.config.floatX
                                                    ),
                                name = 'W',
                                borrow = True
                            )

        self.b = theano.shared(
                                value = np.zeros(
                                    (n_out,),
                                    dtype = theano.config.floatX
                                ),
                                name = 'b',
                                borrow = True
                            )

        self.output = T.nnet.softmax(T.dot(inp, self.W) + self.b)
        self.y_pred = T.argmax(self.output, axis=1)
        self.params = [self.W, self.b]
        self.input = inp

    def NLL(self, y):
        return -T.mean(T.log(self.output)[T.arange(y.shape[0]), y]) 

    def error(self,y):
        return T.mean(T.neq(self.y_pred, y))



class nn(object):

    def __init__(self, rng, inp, n_in, n_hidden, n_out):
        self.h1 = hiddenLayer(
                                rng = rng,
                                inp = inp,
                                n_in = n_in,
                                n_out = n_hidden
                            )

        self.softMax = softmaxLayer(
                                    inp = self.h1.output,
                                    n_in = n_hidden,
                                    n_out = n_out

                                )

        self.L1 = (
                    abs(self.h1.W).sum()
                    + abs(self.softMax.W).sum()
                )

        self.L2 = (
                    (self.h1.W ** 2).sum() 
                    + (self.softMax.W ** 2).sum()
                )


        self.NLL = self.softMax.NLL
        self.error = self.softMax.error

        self.params = self.h1.params + self.softMax.params
        self.input = inp





def load_data(dataPath):
    data_dir, data_file = os.path.split(dataPath)
    if (not os.path.isfile(dataPath)):
        print("ERROR: Data not found at", dataPath)

    print("Loading data sets")
    with gzip.open(dataPath,'rb') as f:
#    with gzip.open(dataPath,'rb') as f:
        try:
            train_set, valid_set, test_set = pickle.load(f, encoding='latin1')
        except:
            train_set, valid_set, test_set = pickle.load(f)
    # train_set, valid_set, test_set format: tuple(input, target)
    # input is a numpy.ndarray of 2 dimensions (a matrix)
    # where each row corresponds to an example. target is a
    # numpy.ndarray of 1 dimension (vector) that has the same length as
    # the number of rows in the input. It should give the target
    # to the example with the same index in the input.

    with gzip.open(dataPath, 'rb') as f:
        try:
             train_set, valid_set, test_set = pickle.load(f, encoding='latin1')
        except:
             train_set, valid_set, test_set = pickle.load(f)    


#    pdb.set_trace()
    print("Building shared datasets")
    tr_x, tr_y = train_set#zip(*train_set)
    shared_tr_x = theano.shared(np.asarray(tr_x, dtype = theano.config.floatX), borrow = True)
    shared_tr_y = T.cast(theano.shared(np.asarray(tr_y, dtype = theano.config.floatX), borrow = True), 'int32')
    
    v_x, v_y = valid_set#zip(*train_set)
    shared_v_x = theano.shared(np.asarray(v_x, dtype = theano.config.floatX), borrow = True)
    shared_v_y = T.cast(theano.shared(np.asarray(v_y, dtype = theano.config.floatX), borrow = True), 'int32')

    te_x, te_y = test_set#zip(*train_set)
    shared_te_x = theano.shared(np.asarray(te_x, dtype = theano.config.floatX), borrow = True)
    shared_te_y = T.cast(theano.shared(np.asarray(te_y, dtype = theano.config.floatX), borrow = True), 'int32')

    rval = [(shared_tr_x,shared_tr_y),
            (shared_v_x,shared_v_y),
            (shared_te_x,shared_te_y)            
           ]

    return rval


def train():
    learning_rate = 0.01
    L1_reg = 0.00
    L2_reg = 0.0001
    n_epochs = 1000
    batch_size=20
    n_hidden=5000
    dataPath = "../data/mnist.pkl.gz"

    datasets = load_data(dataPath)

    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    # compute number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')  # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels

    rng = np.random.RandomState(1234)

    model = nn(
                rng = rng,
                inp = x,
                n_in = 28 * 28,
                n_hidden = n_hidden,
                n_out = 10

              )
            
    cost = (
            model.NLL(y) 
            + L1_reg * model.L1
            + L2_reg * model.L2
        )

    test_model = theano.function(
                            inputs = [index],
                            outputs = model.error(y),
                            givens = {
                                        x: test_set_x[index * batch_size:(index + 1) * batch_size],
                                        y: test_set_y[index * batch_size:(index + 1) * batch_size]
                                    }
                        )

    validate_model = theano.function(
                            inputs = [index],
                            outputs = model.error(y),
                            givens = {
                                        x: valid_set_x[index * batch_size:(index + 1) * batch_size],
                                        y: valid_set_y[index * batch_size:(index + 1) * batch_size]
                                    }
                        )



    gparams = [T.grad(cost, param) for param in model.params]

    updates = [
                (param, param - learning_rate * gparam)
                for param, gparam in zip(model.params, gparams)
            ]


    train_model = theano.function(
                            inputs = [index],
                            outputs = cost,
                            updates = updates,
                            givens = {
                                        x: train_set_x[index * batch_size:(index+1)*batch_size],
                                        y: train_set_y[index * batch_size:(index+1)*batch_size]
                                    }
                            )

##########################start training###########################
    patience = 10000
    patience_increase = 2
    improvment_thresh = 0.995 #relative improvement threshold

    validation_frequency = min(n_train_batches, patience // 2)

    best_validation_loss = np.inf
    best_iter = 0
    test_score = 0.0
#    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False

    while (epoch < n_epochs):
        epoch += 1

        for minibatch_index in range (n_train_batches):
            minibatch_avg_cost = train_model(minibatch_index)
            # iteration number
            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter + 1) % validation_frequency == 0:
                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = np.mean(validation_losses)

                print(
                    'epoch %i, minibatch %i/%i, validation error %f %%' %
                    (
                        epoch,
                        minibatch_index + 1,
                        n_train_batches,
                        this_validation_loss * 100.
                    )
                )

                        # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:
                    #improve patience if loss improvement is good enough
                    if (
                        this_validation_loss < best_validation_loss *
                        improvement_threshold
                    ):
                        patience = max(patience, iter * patience_increase)

                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [test_model(i) for i
                                   in range(n_test_batches)]
                    test_score = numpy.mean(test_losses)

                    print(('     epoch %i, minibatch %i/%i, test error of '
                           'best model %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score * 100.))

            if patience <= iter:
                done_looping = True
                break











if __name__ == "__main__":

    train()

        

    
